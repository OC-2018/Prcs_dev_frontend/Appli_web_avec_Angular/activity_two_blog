import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {PostService} from '../services/post.service';
import {Post} from '../models/post.models';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit, OnDestroy {

  @Input() postTitle:     string;
  @Input() postContent:   string;
  @Input() postDate:      string;
  @Input() postLoveIt:    number;
  @Input() index:         number;

  posts: Post[];
  postsSubscription: Subscription;

  constructor(private postService: PostService, private router: Router) { }

  ngOnInit() {
    this.postsSubscription= this.postService.postsSubject.subscribe(
      (posts: Post[]) => {
        this.posts = posts;
      }
    );
    this.postService.emitPosts();
  }

  ngOnDestroy() {
    this.postsSubscription.unsubscribe();
  }

  
  onAddLoveIt()     { this.postService.addLoveIt(this.index); }
  onRemoveLoveIt()  { this.postService.removeLoveIt(this.index); }

  onRemovePost(post: Post) {
    if (confirm('Etes-vous sûr de vouloir supprimer le post intitulé : "' + this.postTitle + '" ?')) {
      this.postService.removePostFromServer(post);
    } else {
      return null;
    }
  }
}
