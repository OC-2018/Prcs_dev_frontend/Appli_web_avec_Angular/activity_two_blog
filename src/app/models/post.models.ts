export class Post {

  public title: string;
  public content: string;
  public loveIts: number;

  constructor(title: string, content: string, loveIts: number ) {
    this.loveIts = loveIts;
    this.content = content;
    this.title = title;
  }
}
