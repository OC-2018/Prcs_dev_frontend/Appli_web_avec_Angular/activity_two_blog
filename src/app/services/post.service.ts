import {Injectable, OnInit} from '@angular/core';
import {Post} from '../models/post.models';
import {Subject} from 'rxjs';
import * as firebase from 'firebase';
import DataSnapshot = firebase.database.DataSnapshot;
import {post} from 'selenium-webdriver/http';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  // Model of post array
  // postsArray = [
  //   {
  //   title: 'Actualités du mois de Mai 2018',
  //   content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus sit vitae culpa doloremque laborum obcaecati voluptatem molestias ex eaque unde totam, veritatis tempore expedita dolore vel et error corporis minus.',
  //   loveIts: 0
  // },
  //   {
  //     title: 'Actualités du mois de Juin 2018',
  //     content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus sit vitae culpa doloremque laborum obcaecati voluptatem molestias ex eaque unde totam, veritatis tempore expedita dolore vel et error corporis minus.',
  //     loveIts: 0
  //   },
  //   {
  //     title: 'Actualités du mois de Juillet 2018',
  //     content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus sit vitae culpa doloremque laborum obcaecati voluptatem molestias ex eaque unde totam, veritatis tempore expedita dolore vel et error corporis minus.',
  //     loveIts: 0
  //   }
  // ];

  posts: Post[] = [];
  postsSubject = new Subject<Post[]>();
  emitPosts() {this.postsSubject.next(this.posts); }

  constructor() {
    this.getPosts();
  }

  
  /**
   * Add a love It from a post
   * @param i: string
   */
  addLoveIt(i: number)    {
    this.posts[i].loveIts ++;
  }

  /**
   * Remove a love It from a post
   * @param i: string
   */
  removeLoveIt(i: number) {
    this.posts[i].loveIts --;
  }

  /**
   * Delete a post from Posts Array
   * @param i: number
   */
  removePostInLocalArray(i: number)   { this.posts.splice(i, 1); }


  /* ####### FIREBASE ########### */
  savePosts() {
    firebase.database().ref('/posts').set(this.posts);
  }

  getPosts() {
    firebase.database().ref('/posts')
      .on('value', (data: DataSnapshot) => {
        this.posts = data.val() ? data.val() : [];
        this.emitPosts();
      });
  }

  getSinglePost(id: number) {
    return new Promise(
      (resolve, reject) => {
        firebase.database().ref('/posts/' + id).once('value').then(
          (data: DataSnapshot) => {
            resolve( data.val() );
          },
          (error) => {
            reject(error);
          }
        );
      }
    );
  }

  createNewPost(newPost: Post) {
    this.posts.push(newPost);
    this.savePosts();
    this.emitPosts();
  }

  removePostFromServer(postShadow: Post) {
    const postIndexToRemove = this.posts.findIndex(
      (postEl) => {
        if (postEl === postShadow) {
          return true;
        }
      }
    );
    this.posts.splice(postIndexToRemove, 1);
    this.savePosts();
    this.emitPosts();
  }

}
