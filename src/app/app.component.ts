import {Component, OnInit} from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit  {

  constructor() {
    // Initialize Firebase
    const config = {
      apiKey: 'AIzaSyAxhdOQNq4WNJx2zpZPDhOEaM_qaVgOFXI',
      authDomain: 'oc-angular-activity-two.firebaseapp.com',
      databaseURL: 'https://oc-angular-activity-two.firebaseio.com',
      projectId: 'oc-angular-activity-two',
      storageBucket: 'oc-angular-activity-two.appspot.com',
      messagingSenderId: '684234232286'
    };
    firebase.initializeApp(config);

  }

  ngOnInit() {}
}
